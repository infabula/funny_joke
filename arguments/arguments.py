# dit is en module
import sys
#from fun_stuff import funny_joke
import fun_stuff as fs

def main():
    try:
        print("Let's run...")
        fs.funny_joke("John")

        # print all arguments
        for index, arg in enumerate(sys.argv, 1):
            print(index, arg)

        if len(sys.argv) != 2:
            print("Usage: argments <filename>")
            raise ValueError("Geen juiste argumenten")
        else:
            print("Lets open file ", sys.argv[1])
    except ValueError as toe:
        print("Te lang dan weer.")
    except Exception as e:
        print(e)
    finally:
        print("Even opruimen.")

if __name__ == '__main__':
    main()
