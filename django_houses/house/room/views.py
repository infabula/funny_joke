from django.shortcuts import render
from django.http import HttpResponse
from room.models import Room
from django.shortcuts import render

# Create your views here.
def index(request):
    # get all the rooms from db
    rooms = Room.objects.all()
    # put in template data structure
    data =  {
        'rooms': rooms,
    }
    # render template with data
    return render(request, 'room/index.html', data)
    #return HttpResponse(output)
