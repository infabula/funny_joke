from django.db import models


# Create your models here.

class Room(models.Model):
    name = models.CharField(max_length=200, default='unknown')
    length = models.FloatField(default=0.0)
    width = models.FloatField(default=0.0)
    height = models.FloatField(default=0.0)
    nb_of_windows = models.IntegerField(default=0)
